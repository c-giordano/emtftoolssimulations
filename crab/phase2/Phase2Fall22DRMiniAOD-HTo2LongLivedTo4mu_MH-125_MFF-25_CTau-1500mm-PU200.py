
from CRABClient.UserUtilities import config
config = config()

config.section_('General')
config.General.requestName = 'Phase2Fall22DRMiniAOD-HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm-PU200'
config.General.workArea = 'logs'
config.General.transferOutputs = True  ## Do output root files
config.General.transferLogs = True

config.section_('JobType')
config.JobType.psetName = 'test/phase2/pset_Phase2Fall22DRMiniAOD-HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm-PU200.py'
config.JobType.outputFiles = ['out.root'] ## Must be the same as the output file in process.TFileService in config.JobType.psetName python file
config.JobType.pluginName = 'Analysis'
config.JobType.numCores = 8
config.JobType.maxMemoryMB = 8000

config.section_('Data')
config.Data.inputDataset = '/HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm_TuneCP5_14TeV-pythia8/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD'
config.Data.splitting = 'Automatic'
config.Data.totalUnits = 150000
config.Data.publication = False
config.Data.outputDatasetTag = 'Phase2Fall22DRMiniAOD-HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm-PU200'
config.Data.outLFNDirBase = '/store/user/omiguelc/EMTFPP'

config.section_('User')

config.section_('Site')
config.Site.storageSite = 'T3_CH_CERNBOX'
