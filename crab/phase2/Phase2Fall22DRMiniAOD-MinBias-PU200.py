
from CRABClient.UserUtilities import config
config = config()

config.section_('General')
config.General.requestName = 'Phase2Fall22DRMiniAOD-MinBias-PU200'
config.General.workArea = 'logs'
config.General.transferOutputs = True  ## Do output root files
config.General.transferLogs = True

config.section_('JobType')
config.JobType.psetName = 'test/phase2/pset_Phase2Fall22DRMiniAOD-MinBias-PU200.py'
config.JobType.outputFiles = ['out.root'] ## Must be the same as the output file in process.TFileService in config.JobType.psetName python file
config.JobType.pluginName = 'Analysis'
config.JobType.numCores = 8
config.JobType.maxMemoryMB = 15000

config.section_('Data')
config.Data.inputDataset = '/MinBias_TuneCP5_14TeV-pythia8/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 15
config.Data.totalUnits = 12000
config.Data.publication = False
config.Data.outputDatasetTag = 'Phase2Fall22DRMiniAOD-MinBias-PU200'
config.Data.outLFNDirBase = '/store/user/omiguelc/EMTFPP'

config.section_('User')

config.section_('Site')
config.Site.storageSite = 'T3_CH_CERNBOX'
