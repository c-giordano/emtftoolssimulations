mkdir -p test/phase2
mkdir -p crab/phase2

genfiles() {
    # Define variables
    crabtemp=`cat templates/crab_phase2.py`
    output_path="test/phase2/pset_$samplename.py"

    # Custom
    customise_str='SimGeneral/MixingModule/customiseStoredTPConfig.higherPtTP'
    customise_str="$customise_str,SLHCUpgradeSimulations/Configuration/aging.customise_aging_1000"
    customise_str="$customise_str,L1Trigger/Configuration/customisePhase2TTOn110.customisePhase2TTOn110"
    customise_str="$customise_str,L1Trigger/L1TMuonEndCapPhase2/config.customise_mc"
    customise_str="$customise_str,EMTFTools/NtupleMaker/config.customise_ntuple"

    customise_cmds="process.TFileService = cms.Service('TFileService', fileName = cms.string('out.root'))"
    customise_cmds="$customise_cmds\nprocess.gemRecHits.gemDigiLabel = 'simMuonGEMDigis'"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.EMTFP2SimInfoEnabled = cms.bool(False)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.L1TrackTriggerTracksEnabled = cms.bool(False)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.TrackingParticlesEnabled = cms.bool(True)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.GenParticlesEnabled = cms.bool(False)"

    # Generate Pset
    cmsDriver.py "$genfragment_cff" \
        -n 1000 \
        -s GEN,SIM,DIGI,L1TrackTrigger,L1 \
        --nThreads 8 \
        --conditions auto:phase2_realistic --era Phase2C17I13M9 --geometry Extended2026D88 \
        --fileout "file:out.root" --eventcontent FEVTDEBUGHLT \
        --pileup NoPileUp --beamspot HLLHC14TeV --datatier GEN-SIM-DIGI-RAW \
        --customise "$customise_str" \
        --customise_commands="$customise_cmds" \
        --python_filename "$output_path" \
        --no_exec \
        --mc

    # Generate Crab file
    crabcont=`echo "$crabtemp" | sed -e 's/\$SAMPLE_NAME/'"${samplename}/g"`
    echo "$crabcont" > "crab/phase2/$samplename.py"
}

# User Inputs
# samplename="SingleMuon_PosEnd_FlatInvpt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_PosEnd_FlatInvpt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

samplename="SingleMuon_NegEnd_FlatInvpt_2GeV"
genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_NegEnd_FlatInvpt_2GeV_cfi.py"
genfiles "$samplename" "$genfragment_cff"

# samplename="XTo2LongLivedTo4Mu"
# genfragment_cff="EMTFTools/ParticleGuns/LLP_flatMass_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="SingleMuon_PosEnd_FlatPt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_PosEnd_FlatPt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="SingleMuon_NegEnd_FlatPt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_NegEnd_FlatPt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-5000mm_TuneCP5_13p6TeV_pythia8"
# genfragment_cff="EMTFTools/Simulations/genfragments/${samplename}_cff.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-500mm_TuneCP5_13p6TeV_pythia8"
# genfragment_cff="EMTFTools/Simulations/genfragments/${samplename}_cff.py"
# genfiles "$samplename" "$genfragment_cff"

