mkdir -p test/phase2
mkdir -p crab/phase2

genfiles() {
    # Unpack Args
    samplename="$1"
    datasetdas="$2"

    # Dummy?
    filein="/store/mc/Phase2Fall22DRMiniAOD/MinBias_TuneCP5_14TeV-pythia8/GEN-SIM-DIGI-RAW-MINIAOD/PU200_125X_mcRun4_realistic_v2-v1/30000/001ec7cf-71d4-4eb4-9002-078d21560b2f.root"

    # Define variables
    crabtemp=`cat templates/crab_das_phase2.py`
    output_path="test/phase2/pset_$samplename.py"

    # Custom
    customise_str='SimGeneral/MixingModule/customiseStoredTPConfig.higherPtTP'
    customise_str="$customise_str,SLHCUpgradeSimulations/Configuration/aging.customise_aging_1000"
    customise_str="$customise_str,L1Trigger/Configuration/customisePhase2TTOn110.customisePhase2TTOn110"
    customise_str="$customise_str,L1Trigger/L1TMuonEndCapPhase2/config.customise_mc"
    customise_str="$customise_str,EMTFTools/NtupleMaker/config.customise_ntuple"

    customise_cmds="process.TFileService = cms.Service('TFileService', fileName = cms.string('out.root'))"
    customise_cmds="$customise_cmds\nprocess.gemRecHits.gemDigiLabel = 'simMuonGEMDigis'"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.EMTFP2SimInfoEnabled = cms.bool(False)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.L1TrackTriggerTracksEnabled = cms.bool(False)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.TrackingParticlesEnabled = cms.bool(True)"
    customise_cmds="$customise_cmds\nprocess.emtfToolsNtupleMaker.GenParticlesEnabled = cms.bool(False)"

    # Generate Pset
    cmsDriver.py step1 \
        -n 1000 \
        -s L1TrackTrigger,L1 \
        --nThreads 8 \
        --conditions auto:phase2_realistic --era Phase2C17I13M9 --geometry Extended2026D88 \
        --fileout "file:out.root" --eventcontent FEVTDEBUGHLT \
        --filein "$filein" \
        --datatier GEN-SIM-DIGI-RAW \
        --customise "$customise_str" \
        --customise_commands="$customise_cmds" \
        --python_filename "$output_path" \
        --no_exec \
        --mc

    # Generate Crab file
    crabcont=`echo "$crabtemp" | sed -e 's/\$SAMPLE_NAME/'"${samplename}/g"`
    crabcont=`echo "$crabcont" | sed -e 's#\$DATASET_DAS#'"${datasetdas}#g"`
    echo "$crabcont" > "crab/phase2/$samplename.py"
}

# User Inputs
# samplename="Phase2Fall22DRMiniAOD-MinBias-PU200"
# datasetdas="/MinBias_TuneCP5_14TeV-pythia8/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"

# samplename="Phase2Fall22DRMiniAOD-SingleMuon_Pt-0To200-noPU"
# datasetdas="/SingleMuon_Pt-0To200_Eta-1p4To3p1-gun/Phase2Fall22DRMiniAOD-noPU_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"

# samplename="Phase2Fall22DRMiniAOD-SingleMuon_Pt-0To200-noPU"
# datasetdas="/SingleMuon_Pt-0To200_Eta-1p4To3p1-gun/Phase2Fall22DRMiniAOD-noPU_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"
#
# samplename="Phase2Fall22DRMiniAOD-SingleMuon_Pt-0To200-PU200"
# datasetdas="/SingleMuon_Pt-0To200_Eta-1p4To3p1-gun/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"

samplename="Phase2Fall22DRMiniAOD-DoubleMuon_FlatPt-1To100-noPU"
datasetdas="/DoubleMuon_FlatPt-1To100-gun/Phase2Fall22DRMiniAOD-noPU_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
genfiles "$samplename" "$datasetdas"

samplename="Phase2Fall22DRMiniAOD-DoubleMuon_FlatPt-1To100-PU200"
datasetdas="/DoubleMuon_FlatPt-1To100-gun/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
genfiles "$samplename" "$datasetdas"

# samplename="Phase2Fall22DRMiniAOD-HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm-noPU"
# datasetdas="/HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm_TuneCP5_14TeV-pythia8/Phase2Fall22DRMiniAOD-noPU_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"
#
# samplename="Phase2Fall22DRMiniAOD-HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm-PU200"
# datasetdas="/HTo2LongLivedTo4mu_MH-125_MFF-25_CTau-1500mm_TuneCP5_14TeV-pythia8/Phase2Fall22DRMiniAOD-PU200_125X_mcRun4_realistic_v2-v1/GEN-SIM-DIGI-RAW-MINIAOD"
# genfiles "$samplename" "$datasetdas"

